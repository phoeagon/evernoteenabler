package com.kisslab.evernote.enabler;

import static de.robv.android.xposed.XposedHelpers.findAndHookMethod;
import android.graphics.Color;
import android.widget.TextView;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XC_MethodReplacement;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

class k{
};
public class Enabler implements IXposedHookLoadPackage {
	public void handleLoadPackage(LoadPackageParam lpparam) throws Throwable {
		//XposedBridge.log(lpparam.packageName);
		if (!lpparam.packageName.equals("com.evernote"))
			return;
		XposedBridge.log("we are in Evernote!");
		
        findAndHookMethod("com.evernote.client.b",
        		lpparam.classLoader, 
        		"ao",
        		new XC_MethodReplacement() {  //create a method replacer object, as we are going to REPLACE an entire method within the mms app.
                    protected Object replaceHookedMethod( //we make an object here, that passes in the parameters of what to actually change
                            XC_MethodHook.MethodHookParam paramAnonymousMethodHookParam)
                            throws Throwable {
                        return Integer.valueOf(2);
                    }
                });
        findAndHookMethod("com.evernote.client.b",
        		lpparam.classLoader, 
        		"e",
        		int.class,boolean.class,
        		new XC_MethodReplacement() {  //create a method replacer object, as we are going to REPLACE an entire method within the mms app.
                    protected Object replaceHookedMethod( //we make an object here, that passes in the parameters of what to actually change
                            XC_MethodHook.MethodHookParam paramAnonymousMethodHookParam)
                            throws Throwable {
                    	return null;
                    }
                });
	}
}
